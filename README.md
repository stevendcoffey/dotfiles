dotfiles
========

This repository contains standard dotfiles for our organization. The structure is compatible with [rcm](https://github.com/thoughtbot/rcm) which is a dotfile manager.

# To use:
```
git clone git@gitlab.com:stevendcoffey/dotfiles.git ~/.dotfiles
brew tap thoughtbot/formulae
brew install rcm
rcup
```

See https://github.com/thoughtbot/rcm for more details on use of `rcm`.
